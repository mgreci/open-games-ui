import React, { useState, useEffect } from "react";
import { Container } from "@material-ui/core";
import openSocket from "socket.io-client";
import ChooseWord from "./ChooseWord";
import Gameover from "./Gameover";
import Guessing from "./Guessing";
import Header from "./Header";
import Register from "./Register";
import Users from "./Users";
import Storage from "./Storage";

const { REACT_APP_HOST, REACT_APP_PORT } = process.env;

const HOST = REACT_APP_HOST || "localhost";
const PORT = REACT_APP_PORT || 3001;
const URL = `http://${HOST}:${PORT}`;
const socket = openSocket(URL);

const Game = () => {
	const [ name, setName ] = useState(Storage.get("og.name") || "");
	const [ users, setUsers ] = useState(Storage.get("og.users") || []);
	const [ gameHistory, setGameHistory ] = useState(Storage.get("og.gameHistory") || null);
	const [ gameStatus, setGameStatus ] = useState(Storage.get("og.game") || "waiting");
	const [ hangman, setHangman ] = useState(Storage.get("og.hangman") || null);
	const [ hangmanWins, setHangmanWins ] = useState(Storage.get("og.hangmanWins") || false);
	const [ maskedWord, setMaskedWord ] = useState(Storage.get("og.maskedWord") || null);
	const [ guessHistory, setGuessHistory ] = useState(Storage.get("og.guessHistory") || []);
	const [ guessesRemaining, setGuessesRemaining ] = useState(Storage.get("og.guessesRemaining") || 7);

	useEffect(() => {
		socket.on("users", users => {
			setUsers(users);
			Storage.save("og.users", users);
		});

		socket.on("gameHistory", history => {
			setGameHistory(history);
			Storage.save("og.gameHistory", history);
		});

		socket.on("game", status => {
			setGameStatus(status);
			Storage.save("og.game", status);
		});

		socket.on("hangman", user => {
			setHangman(user);
			Storage.save("og.hangman", user);
		});

		socket.on("hangmanWins", hangmanWins => {
			setHangmanWins(hangmanWins);
			Storage.save("og.hangmanWins", hangmanWins);
		});
		
		socket.on("maskedWord", word => {
			setMaskedWord(word);
			Storage.save("og.maskedWord", word);
		});

		socket.on("guessHistory", history => {
			setGuessHistory(history);
			Storage.save("og.guessHistory", history);
		});

		socket.on("guessesRemaining", remaining => {
			setGuessesRemaining(remaining);
			Storage.save("og.guessesRemaining", remaining);
		});
	}, []);

	const updateName = value => {
		setName(value);
		Storage.save("og.name", value);
	};

	const register = () => {
		if (name !== "") {
			socket.emit("register", name);
		}
	};

	const start = () => {
		socket.emit("start", true);
	};

	const chooseTheWord = word => {
		socket.emit("word", word);
	};

	const guess = letter => {
		letter = letter.toUpperCase();
		socket.emit("guess", letter);
	};

	let displayed = null;
	
	switch (gameStatus) {
		case "waiting":
			displayed = <Register registered={users.includes(name)} enoughPlayers={users.length > 1} setName={updateName} name={name} onRegister={register} onStart={start}/>;
			break;
		case "chooseWord":
			displayed = <ChooseWord isHangman={hangman === name} onSelectWord={chooseTheWord}/>;
			break;
		case "guessing":
			displayed = <Guessing isHangman={hangman === name} maskedWord={maskedWord} history={guessHistory} remaining={guessesRemaining} onGuess={guess}/>;
			break;
		case "gameover":
			displayed = <Gameover userWins={(hangmanWins && name === hangman) || (!hangmanWins && name !== hangman)} onStart={start}/>;
			break;
		default:
			displayed = <h1>Unknown game status: {gameStatus}</h1>;
			break;
	}
	
	return (
		<>
			<Header name={name} history={gameHistory && gameHistory[name] ? gameHistory[name] : null}/>
			<Container>
				<Users users={users} me={name} hangman={hangman}/>
				{displayed}
			</Container>
		</>
	);
};

export default Game;