import React from "react";
import { Box, Button } from "@material-ui/core";
import PropTypes from "prop-types";

const Start = ({ onStart }) => (
	<Box display="flex" justifyContent="center">
		<Button variant='contained' color="primary" onClick={onStart}>Start Game</Button>
	</Box>
);

Start.propTypes = {
	onStart: PropTypes.func.isRequired,
};

export default Start;