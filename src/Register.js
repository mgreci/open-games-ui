import React from "react";
import { Box, Typography, Grid, TextField, InputAdornment, IconButton, CircularProgress } from "@material-ui/core";
import { PersonAdd as PersonAddIcon } from "@material-ui/icons";
import PropTypes from "prop-types";
import Start from "./Start";

const Register = ({ registered, enoughPlayers, setName, name, onRegister, onStart }) => {
	const handleKeyPress = event => {
		if (event.charCode === 13) {
			onRegister();
		}
	};

	if (registered) {
		if (enoughPlayers) {
			return <Start onStart={onStart}/>;
		} else {
			return (
				<Box textAlign="center">
					<Typography variant="body1">Waiting for more players</Typography>
					<CircularProgress/>
				</Box>
			);
		}
	}

	return (
		<Box textAlign="center">
			<Typography variant="h4">Who is playing?</Typography>
			<Grid container direction="row" justify="center">
				<Grid item xs={12} md={8}>
					<TextField
						onChange={e => setName(e.target.value)}
						value={name}
						variant="outlined"
						autoFocus
						onKeyPress={handleKeyPress}
						fullWidth
						InputProps={{
							endAdornment: (
								<InputAdornment position="end">
									<IconButton onClick={onRegister}>
										<PersonAddIcon/>
									</IconButton>
								</InputAdornment>
							)
						}}
					/>
				</Grid>
			</Grid>
		</Box>
	);
};

Register.propTypes = {
	registered: PropTypes.bool.isRequired,
	enoughPlayers: PropTypes.bool.isRequired,
	setName: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	onRegister: PropTypes.func.isRequired,
	onStart: PropTypes.func.isRequired,
};

export default Register;