import React from "react";
import { Grid, Button } from "@material-ui/core";
import PropTypes from "prop-types";
import Hangman from "./Hangman";
import MaskedWord from "./MaskedWord";

const Guessing = ({ isHangman, maskedWord, history, remaining, onGuess }) => {
	if (isHangman) {
		return (
			<h4>You are the hangman, wait for other players to guess</h4>
		);
	}

	const guessOptions = 
		["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"].map(char => (
			<Grid key={char} item>
				<Button
					variant="contained"
					disabled={history.includes(char.toUpperCase())}
					onClick={() => onGuess(char)}
				>{char.toUpperCase()}</Button>
			</Grid>
		));

	return (
		<>
			<Hangman remaining={remaining}/>
			<MaskedWord word={maskedWord}/>
			<Grid container direction="row" spacing={1}>
				{guessOptions}
			</Grid>
		</>
	);
};

Guessing.propTypes = {
	isHangman: PropTypes.bool.isRequired,
	maskedWord: PropTypes.arrayOf(PropTypes.string),
	history: PropTypes.arrayOf(PropTypes.string).isRequired,
	remaining: PropTypes.number.isRequired,
	onGuess: PropTypes.func.isRequired,
};

export default Guessing;