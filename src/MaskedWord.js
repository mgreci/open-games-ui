import React from "react";
import { Box, Grid } from "@material-ui/core";
import PropTypes from "prop-types";

const MaskedWord = ({ word }) => {
	const results = word == null ? <h1>missing masked word</h1> : word.map((character, index) => 
		<Grid item key={index}>
			<Box borderBottom={2} style={{fontFamily: "courier"}}>
				{character}
			</Box>
		</Grid>
	);

	return (
		<Box border={1} p={2} mb={2}>
			<Grid container direction="row" justify="center" alignItems="center" spacing={2}>
				{results}
			</Grid>
		</Box>
	);
};

MaskedWord.propTypes = {
	word: PropTypes.arrayOf(PropTypes.string),
};

export default MaskedWord;