import React, { useState } from "react";
import { Box, Typography, TextField, InputAdornment, IconButton, CircularProgress } from "@material-ui/core";
import { VpnKey as VpnKeyIcon } from "@material-ui/icons";
import PropTypes from "prop-types";

const ChooseWord = ({ isHangman, onSelectWord }) => {
	const [ word, setWord ] = useState("");

	const onChange = value => {
		// remove non-alphabetic characters
		value = value.replace(/[^a-z\s]/gi, "");

		// remove multiple spaces
		value = value.replace(/\s{2,}/gi, " ");

		// UPPERCASE
		value = value.toUpperCase();

		setWord(value);
	};

	const handleSelectWord = event => {
		switch (event.type) {
			case "click":
				onSelectWord(word.trim());
				break;
			case "keypress":
				if (event.charCode === 13) {
					onSelectWord(word.trim());
				}
				break;
			default:
				console.warn("unknown event type: ", event.type);
				break;
		}
	};

	const message = !isHangman ? "Waiting for the hangman to choose a word/phrase" : "You are the hangman, choose a word/phrase";

	return (
		<Box textAlign="center">
			<Typography variant="body1">{message}</Typography>
			{isHangman ? (
				<TextField
					onChange={e => onChange(e.target.value)}
					value={word}
					variant="outlined"
					autoFocus
					onKeyPress={handleSelectWord}
					fullWidth
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconButton onClick={handleSelectWord}>
									<VpnKeyIcon/>
								</IconButton>
							</InputAdornment>
						)
					}}
				/>
			) : <CircularProgress/>}
		</Box>
	);
};

ChooseWord.propTypes = {
	isHangman: PropTypes.bool.isRequired,
	onSelectWord: PropTypes.func.isRequired,
};

export default ChooseWord;