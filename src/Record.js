import React from "react";
import { Typography } from "@material-ui/core";
import PropTypes from "prop-types";

const Record = ({ record }) => {
	if (record === null) {
		return "No Game Record Yet";
	}
	
	const { hangman, guessing } = record;
	return (
		<>
			<Typography variant="body1">Hangman: {hangman["wins"]}-{hangman["loses"]}</Typography>
			<Typography variant="body1">Guesser: {guessing["wins"]}-{guessing["loses"]}</Typography>
		</>
	);
};

Record.propTypes = {
	record: PropTypes.shape({
		hangman: PropTypes.shape({
			wins: PropTypes.number,
			loses: PropTypes.number,
		}),
		guessing: PropTypes.shape({
			wins: PropTypes.number,
			loses: PropTypes.number,
		}),
	})
};

export default Record;