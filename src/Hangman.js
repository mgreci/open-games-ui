import React from "react";
import { Grid, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

const PHRASE = "HANGMAN";

const Hangman = ({ remaining }) => (
	<Grid container direction="row" spacing={2} alignItems="center" justify="center">
		{PHRASE.split("").map((character, index) => 
			<Grid item key={index}>
				<Typography variant="h1" color={index < remaining ? "primary" : "secondary"}>{character}</Typography>
			</Grid>
		)}
	</Grid>
);

Hangman.propTypes = {
	remaining: PropTypes.number.isRequired,
};

export default Hangman;