import React from "react";
import { AppBar, Toolbar, Box, Typography, Tooltip } from "@material-ui/core";
import PropTypes from "prop-types";
import Record from "./Record";

const Header = ({ name, history }) => (
	<AppBar position="static">
		<Toolbar>
			<Box width={1} display='flex' alignItems='center' justifyContent='space-between'>
				<Typography variant="h6">Hangman</Typography>
				<Tooltip title={<Record record={history}/>}>
					<Typography variant="h6">{name}</Typography>
				</Tooltip>
			</Box>
		</Toolbar>
	</AppBar>
);

Header.propTypes = {
	name: PropTypes.string.isRequired,
	history: PropTypes.object,
};

export default Header;