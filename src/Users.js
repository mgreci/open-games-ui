import React from "react";
import { Box, Grid } from "@material-ui/core";
import { Accessibility as AccessibilityIcon } from "@material-ui/icons";
import PropTypes from "prop-types";
import { isMobile } from "react-device-detect";

const Users = ({ users, me, hangman }) => (
	<Box my={2}>
		<Grid container direction="row" spacing={2}>
			{users.map((user, index) => 
				<Grid item key={index}>
					<Box px={2} py={isMobile ? 2 : 1} color="white" bgcolor={user === me ? "primary.main" : "text.disabled"} borderRadius={isMobile ? 20 : 15} display="flex" alignItems="center">
						{hangman === user && <AccessibilityIcon/>}
						{user}
					</Box>
				</Grid>
			)}
		</Grid>
	</Box>
);

Users.propTypes = {
	users: PropTypes.arrayOf(PropTypes.string).isRequired,
	me: PropTypes.string.isRequired,
	hangman: PropTypes.string,
};

export default Users;