import React from "react";
import PropTypes from "prop-types";
import Start from "./Start";
import { Box, Typography } from "@material-ui/core";

const Gameover = ({ userWins, onStart }) => (
	<Box textAlign="center">
		<Typography variant="h5">Game Over</Typography>
		<Typography variant="h1">You {userWins ? "Win" : "Lose"}!</Typography>
		<Start onStart={onStart}/>
	</Box>
);

Gameover.propTypes = {
	userWins: PropTypes.bool.isRequired,
	onStart: PropTypes.func.isRequired,
};

export default Gameover;